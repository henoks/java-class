import java.lang.Math;
public class Fraction 
{
    private int denominator;
    private int numerator;
    static int gcd(int a, int b) {
        a = Math.abs(a);
        b = Math.abs(b);
        while (a != b) {
            if (a > b) a = a - b;
            else b = b - a;
        }
        return a;
    }
    public static Fraction add(Fraction frac1, Fraction frac2){
        int newNumer = frac1.numerator * frac2.denominator + frac2.numerator*frac1.denominator;
        int newDenom = frac1.denominator * frac2.denominator;
        Fraction sum = new Fraction;
        sum.numerator = newNumer / gcd(newNumer, newDenom);
        sum.denominator = newDenom / gcd(newNumer, newDenom);
        return sum;
    }
    public static Fraction multiply(Fraction frac1, Fraction frac2){
        Fraction product = new Fraction;
        product.numerator = frac1.numerator*frac2.numerator/gcd(frac1.numerator*frac2.numerator);
        product.denominator = frac1.denominator*frac2.denominator/gcd(frac1.denominator*frac2.denominator);
        return product;
    }
    public static Fraction subtract(Fraction frac1, Fraction frac2){
        int newNumer = frac1.numerator*frac2.denominator - frac2.numerator*frac1.denominator;
        int newDenom = frac1.denominator*frac2.denominator;
        Fraction diff = new Fraction;
        diff.numerator = newNumer / gcd(newNumer, newDenom);
        diff.denominator = newDenom / gcd(newNumer, newDenom);
        return diff;
    }
    public static Fraction divide(Fraction frac1, Fraction frac2){
        Fraction quotient = new Fraction;
        quotient.numerator = frac1.numerator*frac2.denominator/gcd(frac1.numerator*frac2.numerator);
        quotient.denominator = frac1.denominator*frac2.numerator/gcd(frac1.denominator*frac2.denominator);
        return quotient;
    }
    
}

